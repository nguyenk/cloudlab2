import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("exampleLocal_stats_history.csv")

plt.figure()

df.iloc[1:,4].plot()

plt.legend(loc='best')

plt.title("Locally deployed numericalintegral with 1 user")

plt.show()

plt.savefig('image.png', dpi=100)

from math import fabs, sin
from flask import Flask
app = Flask(__name__)

def integration(absSinFunc, lower, upper, N):
    dx = (upper - lower) / N
    integral = 0
    for i in range(N):
        xip12 = dx*(i * 0.5)
        dI = absSinFunc(xip12)*dx
        integral += dI
    return integral

def absSin(x):
    return fabs(sin(x)) 

@app.route('/<lower>/<upper>')
def main(lower=0, upper=0):
    lower = float(lower)
    upper = float(upper)
    resultString = ""
    Ns = [10, 100, 100, 1000, 10000, 100000, 1000000]
    for N in Ns:
        result = integration(absSin, lower, upper, N)
        resultString += str(result)  + " "

    return resultString

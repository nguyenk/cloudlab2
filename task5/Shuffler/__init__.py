# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

# Input: List of key-value pair 
# Output: Key-value pair where value is list and key is word
def main(mapperOutput: list) -> str:
    result = {}
    for keyValue in mapperOutput:
        for key in keyValue:
            if key in result:
                result[key].append(keyValue[key])
            else:
                result[key] = [keyValue[key]]
    return result

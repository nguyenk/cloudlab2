# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    # Retrieve the connection string for use with the application. The storage
    # connection string is stored in an environment variable on the machine
    # running the application called AZURE_STORAGE_CONNECTION_STRING. If the environment variable is
    # created after the application is launched in a console or with Visual Studio,
    # the shell or application needs to be closed and reloaded to take the
    # environment variable into account.
    connect_str = "DefaultEndpointsProtocol=https;AccountName=cloudlab2task5;AccountKey=THbtouyuqYOoutOaFDlshInpjfQUfl6I5vREQ41W9FYLLb1Zeu0nWe21n2nQ6C/JeuGfEdOieHx0+ASt95U7pw==;EndpointSuffix=core.windows.net"

    # List of dictionary
    files = yield context.call_activity("GetInputData", connect_str)

    tasks = []
    for file in files:
        tasks.append(context.call_activity("Mapper", file))
    #  List of List of dictionary 
    shufflerInputs = yield context.task_all(tasks)
    
    tasks = []
    for shufflerInput in shufflerInputs:
        tasks += shufflerInput
    
    ReducerInputs = yield context.call_activity("Shuffler", tasks)
    result = yield context.call_activity("Reducer", ReducerInputs)

    return result

main = df.Orchestrator.create(orchestrator_function)
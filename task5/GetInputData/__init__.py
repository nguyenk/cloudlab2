# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

from azure.storage.blob import BlobServiceClient

def main(key: str) -> str:
    result = []
    # Create the BlobServiceClient object
    blob_service_client = BlobServiceClient.from_connection_string(key)
    container_client = blob_service_client.get_container_client(container="mapreduce")

    print("\nListing blobs...")
    # List the blobs in the container
    blob_list = container_client.list_blobs()
    for blob in blob_list:
        keyValuePair = {}
        lines = str(container_client.download_blob(blob.name).readall()).replace("b", "").split('\\r\\n')
        for idx, line in enumerate(lines):
            keyValuePair[idx] = line
        result.append(keyValuePair)

    return result

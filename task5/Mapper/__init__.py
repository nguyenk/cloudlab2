# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import os
from os.path import dirname
from typing import List

# Input: key-value pair where key is line number and value is line content
# output: List of key-value pair with key-value pair items
def main(lines: dict) -> List[str]:
    result = []
    for lineNumber in lines:
        keyValuePair = {}
        words = lines[lineNumber].split()
        for word in words:
            keyValuePair[word.lower()] = 1
        result.append(keyValuePair)
    return result

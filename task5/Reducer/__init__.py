# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

# Input: key-value pair where key is a word value is a list [1,1,1,..]
# Output: key-value pair with value is counted value
def main(words: dict) -> str:
    result = {}
    for word in words:
        result[word] = len(words[word])
    return result

import logging

import azure.functions as func
from math import fabs, sin

def integration(absSinFunc, lower, upper, N):
    dx = (upper - lower) / N
    integral = 0
    for i in range(N):
        xip12 = dx*(i * 0.5)
        dI = absSinFunc(xip12)*dx
        integral += dI
    return integral

def absSin(x):
    return fabs(sin(x))


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = req.params.get('lower')
    upper = req.params.get('upper')

    if not lower or not upper:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            lower = req_body.get('lower')
            upper = req_body.get('upper')

    if lower or upper:
        lower = float(lower)
        upper = float(upper)
        resultString = ""
        Ns = [10, 100, 100, 1000, 10000, 100000, 1000000]
        for N in Ns:
            result = integration(absSin, lower, upper, N)
            resultString += str(result)  + " "

        return func.HttpResponse(resultString)
    else:
        return func.HttpResponse(
             "This HTTP triggered function executed successfully. Pass a lower and upper in the query string or in the request body for a personalized response.",
             status_code=200
        )
